ListPy
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Ploss.Mpath.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ploss.mpath.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ploss_Mpath_ListPy_Frequency.rst