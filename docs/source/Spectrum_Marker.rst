Marker<Marker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.spectrum.marker.repcap_marker_get()
	driver.spectrum.marker.repcap_marker_set(repcap.Marker.Nr1)





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Marker.MarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.marker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Marker_Npeak.rst