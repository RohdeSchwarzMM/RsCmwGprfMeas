All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:EPSensor:STATe:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.ExtPwrSensor.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: