Capture
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:CAPTure



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.Capture.CaptureCls
	:members:
	:undoc-members:
	:noindex: