Scenario
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:GPRF:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:SCENario:CSPath
	ROUTe:GPRF:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwGprfMeas.Implementations.Route.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Catalog.rst
	Route_Scenario_Maiq.rst
	Route_Scenario_MaProtocol.rst
	Route_Scenario_Salone.rst