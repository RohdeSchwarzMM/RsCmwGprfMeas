Initiate
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Initiate.InitiateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.initiate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Initiate_Ploss.rst