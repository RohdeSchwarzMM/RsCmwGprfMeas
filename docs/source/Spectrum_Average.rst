Average
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Average_Average.rst
	Spectrum_Average_Current.rst
	Spectrum_Average_Maximum.rst
	Spectrum_Average_Minimum.rst