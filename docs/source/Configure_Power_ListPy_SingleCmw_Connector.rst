Connector
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CONNector
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CONNector:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CONNector
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CONNector:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ListPy.SingleCmw.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex: