Bandwidth
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BANDpass:BWIDth
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BANDpass:BWIDth:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BANDpass:BWIDth
	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET:FILTer:BANDpass:BWIDth:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ParameterSetList.FilterPy.Bandpass.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: