Sensor<Sensor>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.nrpm.sensor.repcap_sensor_get()
	driver.configure.nrpm.sensor.repcap_sensor_set(repcap.Sensor.Nr1)





.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Nrpm.Sensor.SensorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrpm.sensor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Nrpm_Sensor_Frequency.rst