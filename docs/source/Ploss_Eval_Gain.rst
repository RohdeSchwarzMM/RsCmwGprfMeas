Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:GAIN

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:GAIN



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: