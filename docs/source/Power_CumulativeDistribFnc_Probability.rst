Probability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:PROBability

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:PROBability



.. autoclass:: RsCmwGprfMeas.Implementations.Power.CumulativeDistribFnc.Probability.ProbabilityCls
	:members:
	:undoc-members:
	:noindex: