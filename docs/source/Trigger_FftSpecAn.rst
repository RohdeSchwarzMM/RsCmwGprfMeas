FftSpecAn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:SOURce
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OMODe
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:TOUT
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:SLOPe

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:SOURce
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OMODe
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:MGAP
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:TOUT
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:THReshold
	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:SLOPe



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_FftSpecAn_Catalog.rst
	Trigger_FftSpecAn_OsStop.rst