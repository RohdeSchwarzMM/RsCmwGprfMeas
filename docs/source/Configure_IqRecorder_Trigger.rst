Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TRIGger:SOURce

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TRIGger:SOURce



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: