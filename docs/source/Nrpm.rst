Nrpm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:NRPM
	single: STOP:GPRF:MEASurement<Instance>:NRPM
	single: ABORt:GPRF:MEASurement<Instance>:NRPM

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:NRPM
	STOP:GPRF:MEASurement<Instance>:NRPM
	ABORt:GPRF:MEASurement<Instance>:NRPM



.. autoclass:: RsCmwGprfMeas.Implementations.Nrpm.NrpmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrpm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Nrpm_Sensor.rst
	Nrpm_State.rst