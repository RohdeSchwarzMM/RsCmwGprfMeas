Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:MINimum:MINimum



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Minimum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: