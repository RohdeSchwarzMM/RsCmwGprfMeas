IqRecorder
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MODE
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:USER
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MODE
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:RATio
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FORMat
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:MUNit
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:USER
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:IQFile
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:WTFile



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_IqRecorder_Capture.rst
	Configure_IqRecorder_FilterPy.rst
	Configure_IqRecorder_ListPy.rst
	Configure_IqRecorder_Trigger.rst