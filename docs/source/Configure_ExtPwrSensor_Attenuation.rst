Attenuation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation:STATe
	single: CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation:STATe
	CONFigure:GPRF:MEASurement<Instance>:EPSensor:ATTenuation



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.ExtPwrSensor.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: