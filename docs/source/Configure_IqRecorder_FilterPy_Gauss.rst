Gauss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:GAUSs:BWIDth



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.FilterPy.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex: