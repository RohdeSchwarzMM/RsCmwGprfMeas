ZeroSpan
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:SWT

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:ZSPan:SWT



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.ZeroSpan.ZeroSpanCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spectrum.zeroSpan.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Spectrum_ZeroSpan_Rbw.rst
	Configure_Spectrum_ZeroSpan_Vbw.rst