State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ploss.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ploss_State_All.rst