Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:TRACe:GAIN

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:TRACe:GAIN



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.Trace.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: