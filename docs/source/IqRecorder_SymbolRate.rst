SymbolRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:SRATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:SRATe



.. autoclass:: RsCmwGprfMeas.Implementations.IqRecorder.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: