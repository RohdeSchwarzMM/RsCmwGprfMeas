Short
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:SHORt

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:SHORt



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Short.ShortCls
	:members:
	:undoc-members:
	:noindex: