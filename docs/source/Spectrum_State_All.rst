All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:STATe:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: