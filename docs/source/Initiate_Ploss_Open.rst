Open
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:PLOSs:OPEN

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:PLOSs:OPEN



.. autoclass:: RsCmwGprfMeas.Implementations.Initiate.Ploss.Open.OpenCls
	:members:
	:undoc-members:
	:noindex: