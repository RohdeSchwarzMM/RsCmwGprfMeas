View
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:VIEW:AFTaps

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:VIEW:AFTaps



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Ploss.View.ViewCls
	:members:
	:undoc-members:
	:noindex: