Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SOURce
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SLOPe
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:TOUT

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SOURce
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:THReshold
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:SLOPe
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:MGAP
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:TOUT



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Spectrum_Catalog.rst