Minimum
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Minimum_Average.rst
	Spectrum_Minimum_Current.rst
	Spectrum_Minimum_Maximum.rst
	Spectrum_Minimum_Minimum.rst