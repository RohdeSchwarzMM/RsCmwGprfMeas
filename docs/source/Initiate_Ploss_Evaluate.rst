Evaluate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:PLOSs:EVALuate

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:PLOSs:EVALuate



.. autoclass:: RsCmwGprfMeas.Implementations.Initiate.Ploss.Evaluate.EvaluateCls
	:members:
	:undoc-members:
	:noindex: