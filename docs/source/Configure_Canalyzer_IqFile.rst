IqFile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:IQFile

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:IQFile



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Canalyzer.IqFile.IqFileCls
	:members:
	:undoc-members:
	:noindex: