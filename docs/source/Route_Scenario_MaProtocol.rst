MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwGprfMeas.Implementations.Route.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: