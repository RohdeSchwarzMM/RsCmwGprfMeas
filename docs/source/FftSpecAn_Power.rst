Power
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.FftSpecAn.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.fftSpecAn.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	FftSpecAn_Power_Average.rst
	FftSpecAn_Power_Current.rst
	FftSpecAn_Power_Maximum.rst
	FftSpecAn_Power_Minimum.rst