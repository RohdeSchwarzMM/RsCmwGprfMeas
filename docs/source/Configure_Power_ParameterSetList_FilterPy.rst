FilterPy
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ParameterSetList.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.parameterSetList.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_ParameterSetList_FilterPy_Bandpass.rst
	Configure_Power_ParameterSetList_FilterPy_Gauss.rst
	Configure_Power_ParameterSetList_FilterPy_TypePy.rst