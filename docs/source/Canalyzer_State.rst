State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:CANalyzer:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:CANalyzer:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.Canalyzer.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.canalyzer.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Canalyzer_State_All.rst