Power
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:POWer
	single: STOP:GPRF:MEASurement<Instance>:POWer
	single: ABORt:GPRF:MEASurement<Instance>:POWer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:POWer
	STOP:GPRF:MEASurement<Instance>:POWer
	ABORt:GPRF:MEASurement<Instance>:POWer



.. autoclass:: RsCmwGprfMeas.Implementations.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_AmplitudeProbDensity.rst
	Power_Average.rst
	Power_CumulativeDistribFnc.rst
	Power_Current.rst
	Power_ElapsedStats.rst
	Power_IqData.rst
	Power_IqInfo.rst
	Power_ListPy.rst
	Power_Maximum.rst
	Power_Minimum.rst
	Power_Peak.rst
	Power_StandardDev.rst
	Power_State.rst