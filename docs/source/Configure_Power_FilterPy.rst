FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:TYPE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:FILTer:TYPE



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_FilterPy_Bandpass.rst
	Configure_Power_FilterPy_Gauss.rst