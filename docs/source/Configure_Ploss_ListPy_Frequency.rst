Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:LIST:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:LIST:FREQuency



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Ploss.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: