RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst32
	# All values (32x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16
	Inst17 | Inst18 | Inst19 | Inst20 | Inst21 | Inst22 | Inst23 | Inst24
	Inst25 | Inst26 | Inst27 | Inst28 | Inst29 | Inst30 | Inst31 | Inst32

Marker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Marker.Nr1
	# Values (2x):
	Nr1 | Nr2

Sensor
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Sensor.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

