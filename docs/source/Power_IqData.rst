IqData
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:IQData
	single: READ:GPRF:MEASurement<Instance>:POWer:IQData

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:IQData
	READ:GPRF:MEASurement<Instance>:POWer:IQData



.. autoclass:: RsCmwGprfMeas.Implementations.Power.IqData.IqDataCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.iqData.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_IqData_Bin.rst