Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:NRPM:SENSor<nr_NRPM>:FREQuency



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Nrpm.Sensor.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: