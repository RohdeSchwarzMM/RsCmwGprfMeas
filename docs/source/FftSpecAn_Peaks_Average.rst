Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:AVERage
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:AVERage

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:AVERage
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:PEAKs:AVERage



.. autoclass:: RsCmwGprfMeas.Implementations.FftSpecAn.Peaks.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: