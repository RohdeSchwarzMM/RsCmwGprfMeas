AmplitudeProbDensity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:APD

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:APD



.. autoclass:: RsCmwGprfMeas.Implementations.Power.AmplitudeProbDensity.AmplitudeProbDensityCls
	:members:
	:undoc-members:
	:noindex: