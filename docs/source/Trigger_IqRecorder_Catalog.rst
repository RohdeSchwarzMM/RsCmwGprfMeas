Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:CATalog:SOURce



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.IqRecorder.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: