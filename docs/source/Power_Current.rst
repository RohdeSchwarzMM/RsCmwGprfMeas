Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:CURRent

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:CURRent
	FETCh:GPRF:MEASurement<Instance>:POWer:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:CURRent



.. autoclass:: RsCmwGprfMeas.Implementations.Power.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_Current_Maximum.rst
	Power_Current_Minimum.rst
	Power_Current_Rms.rst