Ploss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:GPRF:MEASurement<Instance>:PLOSs
	single: ABORt:GPRF:MEASurement<Instance>:PLOSs

.. code-block:: python

	STOP:GPRF:MEASurement<Instance>:PLOSs
	ABORt:GPRF:MEASurement<Instance>:PLOSs



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ploss_Clear.rst
	Ploss_Eval.rst
	Ploss_Open.rst
	Ploss_Short.rst
	Ploss_State.rst