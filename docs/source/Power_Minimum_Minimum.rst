Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:MINimum:MINimum
	single: READ:GPRF:MEASurement<Instance>:POWer:MINimum:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:MINimum:MINimum
	READ:GPRF:MEASurement<Instance>:POWer:MINimum:MINimum



.. autoclass:: RsCmwGprfMeas.Implementations.Power.Minimum.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: