Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:TRACe:FREQuency

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:TRACe:FREQuency



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.Trace.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: