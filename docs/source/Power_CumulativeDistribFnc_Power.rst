Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:POWer

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF:POWer



.. autoclass:: RsCmwGprfMeas.Implementations.Power.CumulativeDistribFnc.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: