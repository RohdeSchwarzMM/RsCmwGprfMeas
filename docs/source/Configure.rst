Configure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:DISPlay

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:DISPlay



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Canalyzer.rst
	Configure_ExtPwrSensor.rst
	Configure_FftSpecAn.rst
	Configure_IqRecorder.rst
	Configure_IqVsSlot.rst
	Configure_Nrpm.rst
	Configure_Ploss.rst
	Configure_Power.rst
	Configure_RfSettings.rst
	Configure_Spectrum.rst