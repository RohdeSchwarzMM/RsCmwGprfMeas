ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:STARt
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:STOP
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:COUNt
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:STARt
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:STOP
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:COUNt
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqVsSlot.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.iqVsSlot.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_IqVsSlot_ListPy_EnvelopePower.rst
	Configure_IqVsSlot_ListPy_Frequency.rst
	Configure_IqVsSlot_ListPy_Retrigger.rst
	Configure_IqVsSlot_ListPy_Sstop.rst