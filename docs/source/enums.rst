Enums
=========

AveragingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AveragingMode.LINear
	# All values (2x):
	LINear | LOGarithmic

CcdfMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CcdfMode.POWer
	# All values (2x):
	POWer | STATistic

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RH8
	# All values (96x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8
	RC1 | RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8
	RD1 | RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8
	RE1 | RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8
	RF1 | RF2 | RF3 | RF4 | RF5 | RF6 | RF7 | RF8
	RG1 | RG2 | RG3 | RG4 | RG5 | RG6 | RG7 | RG8
	RH1 | RH2 | RH3 | RH4 | RH5 | RH6 | RH7 | RH8

Detector
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Detector.AUTopeak
	# All values (6x):
	AUTopeak | AVERage | MAXPeak | MINPeak | RMS | SAMPle

DetectorBasic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetectorBasic.PEAK
	# All values (2x):
	PEAK | RMS

DigitalFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DigitalFilterType.BANDpass
	# All values (5x):
	BANDpass | CDMA | GAUSs | TDSCdma | WCDMa

FileSave
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FileSave.OFF
	# All values (3x):
	OFF | ON | ONLY

FilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterType.B10Mhz
	# All values (5x):
	B10Mhz | B1MHz | GAUSs | NY1Mhz | NYQuist

IqFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqFormat.IQ
	# All values (2x):
	IQ | RPHI

MagnitudeUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MagnitudeUnit.RAW
	# All values (2x):
	RAW | VOLT

MeasTab
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasTab.EPSensor
	# All values (6x):
	EPSensor | FFTSanalyzer | IQRecorder | IQVSlot | POWer | SPECtrum

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.NORMal
	# All values (2x):
	NORMal | TALignment

OffsetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OffsetMode.FIXed
	# All values (2x):
	FIXed | VARiable

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PathIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathIndex.P1
	# All values (8x):
	P1 | P2 | P3 | P4 | P5 | P6 | P7 | P8

PathLossState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathLossState.NCAP
	# All values (3x):
	NCAP | PEND | RDY

PwrSensorResolution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PwrSensorResolution.PD0
	# All values (4x):
	PD0 | PD1 | PD2 | PD3

RbwFilterType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwFilterType.BANDpass
	# All values (2x):
	BANDpass | GAUSs

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RfConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RfConnector.I11I
	# Last value:
	value = enums.RfConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.CSPath
	# All values (5x):
	CSPath | MAIQ | MAPR | SALone | UNDefined

SignalSlopeExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlopeExt.FALLing
	# All values (4x):
	FALLing | FEDGe | REDGe | RISing

SpanMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpanMode.FSWeep
	# All values (2x):
	FSWeep | ZSPan

Statistic
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Statistic.AVERage
	# All values (4x):
	AVERage | CURRent | MAXimum | MINimum

TargetMainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetMainState.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

Timing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Timing.CENTered
	# All values (2x):
	CENTered | STEP

TriggerPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerPowerMode.ALL
	# All values (4x):
	ALL | ONCE | PRESelect | SWEep

TriggerSequenceMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSequenceMode.ONCE
	# All values (2x):
	ONCE | PRESelect

TriggerSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSource.EXTernal
	# All values (4x):
	EXTernal | FREerun | IF | IFPower

UserDebugMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UserDebugMode.DEBug
	# All values (2x):
	DEBug | USER

ZeroingState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ZeroingState.FAILed
	# All values (2x):
	FAILed | PASSed

