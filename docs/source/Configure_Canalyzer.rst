Canalyzer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:MNAMe
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SEGMent
	single: CONFigure:GPRF:MEASurement<Instance>:CANalyzer:STEP

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:MNAMe
	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:SEGMent
	CONFigure:GPRF:MEASurement<Instance>:CANalyzer:STEP



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Canalyzer.CanalyzerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.canalyzer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Canalyzer_IqFile.rst
	Configure_Canalyzer_Sall.rst