Zero
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:GPRF:MEASurement<Instance>:EPSensor:ZERO

.. code-block:: python

	CALibration:GPRF:MEASurement<Instance>:EPSensor:ZERO



.. autoclass:: RsCmwGprfMeas.Implementations.Calibration.ExtPwrSensor.Zero.ZeroCls
	:members:
	:undoc-members:
	:noindex: