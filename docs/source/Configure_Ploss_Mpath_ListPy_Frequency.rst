Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:MPATh:LIST:FREQuency

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:MPATh:LIST:FREQuency



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Ploss.Mpath.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: