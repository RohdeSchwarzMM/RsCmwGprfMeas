Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:FREQuency

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:FREQuency



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: