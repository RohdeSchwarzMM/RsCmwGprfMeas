Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation:CURRent
	single: READ:GPRF:MEASurement<Instance>:POWer:SDEViation:CURRent

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation:CURRent
	READ:GPRF:MEASurement<Instance>:POWer:SDEViation:CURRent



.. autoclass:: RsCmwGprfMeas.Implementations.Power.StandardDev.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: