FftSpecAn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	single: ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:FFTSanalyzer
	STOP:GPRF:MEASurement<Instance>:FFTSanalyzer
	ABORt:GPRF:MEASurement<Instance>:FFTSanalyzer



.. autoclass:: RsCmwGprfMeas.Implementations.FftSpecAn.FftSpecAnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.fftSpecAn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	FftSpecAn_Icomponent.rst
	FftSpecAn_Peaks.rst
	FftSpecAn_Power.rst
	FftSpecAn_Qcomponent.rst
	FftSpecAn_State.rst