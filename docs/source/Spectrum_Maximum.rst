Maximum
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Maximum_Average.rst
	Spectrum_Maximum_Current.rst
	Spectrum_Maximum_Maximum.rst
	Spectrum_Maximum_Minimum.rst