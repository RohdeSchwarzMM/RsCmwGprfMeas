Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:SCENario:CATalog:CSPath

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:SCENario:CATalog:CSPath



.. autoclass:: RsCmwGprfMeas.Implementations.Route.Scenario.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: