Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:SPECtrum:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:SPECtrum:CATalog:SOURce



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.Spectrum.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: