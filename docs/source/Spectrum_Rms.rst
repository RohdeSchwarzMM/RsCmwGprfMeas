Rms
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.rms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Rms_Average.rst
	Spectrum_Rms_Current.rst
	Spectrum_Rms_Maximum.rst
	Spectrum_Rms_Minimum.rst