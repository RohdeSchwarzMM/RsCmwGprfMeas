Canalyzer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:CANalyzer
	single: STOP:GPRF:MEASurement<Instance>:CANalyzer
	single: ABORt:GPRF:MEASurement<Instance>:CANalyzer

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:CANalyzer
	STOP:GPRF:MEASurement<Instance>:CANalyzer
	ABORt:GPRF:MEASurement<Instance>:CANalyzer



.. autoclass:: RsCmwGprfMeas.Implementations.Canalyzer.CanalyzerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.canalyzer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Canalyzer_State.rst