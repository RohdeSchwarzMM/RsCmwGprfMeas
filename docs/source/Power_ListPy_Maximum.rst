Maximum
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Power.ListPy.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.listPy.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_ListPy_Maximum_Current.rst