Sstop
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SSTop

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:SSTop



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.ListPy.Sstop.SstopCls
	:members:
	:undoc-members:
	:noindex: