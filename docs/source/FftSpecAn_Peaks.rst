Peaks
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.FftSpecAn.Peaks.PeaksCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.fftSpecAn.peaks.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	FftSpecAn_Peaks_Average.rst
	FftSpecAn_Peaks_Current.rst