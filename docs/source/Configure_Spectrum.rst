Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:AMODe
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:REPetition
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:TOUT
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:SCOunt



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Spectrum_FreqSweep.rst
	Configure_Spectrum_Frequency.rst
	Configure_Spectrum_ZeroSpan.rst