Peak
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Power.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_Peak_Maximum.rst
	Power_Peak_Minimum.rst