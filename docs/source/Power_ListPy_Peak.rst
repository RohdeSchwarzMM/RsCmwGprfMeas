Peak
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Power.ListPy.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.listPy.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_ListPy_Peak_Maximum.rst
	Power_ListPy_Peak_Minimum.rst