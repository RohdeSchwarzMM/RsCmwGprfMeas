FilterPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:FILTer:TYPE



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.FilterPy.FilterPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.iqRecorder.filterPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_IqRecorder_FilterPy_Bandpass.rst
	Configure_IqRecorder_FilterPy_Gauss.rst