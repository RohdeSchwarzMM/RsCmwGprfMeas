All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRPM:STATe:ALL

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRPM:STATe:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Nrpm.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: