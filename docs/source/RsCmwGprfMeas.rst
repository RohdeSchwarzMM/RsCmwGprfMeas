RsCmwGprfMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwGprfMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst32
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwGprfMeas.RsCmwGprfMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Calibration.rst
	Canalyzer.rst
	Configure.rst
	ExtPwrSensor.rst
	FftSpecAn.rst
	Initiate.rst
	IqRecorder.rst
	IqVsSlot.rst
	Nrpm.rst
	Ploss.rst
	Power.rst
	Route.rst
	Spectrum.rst
	Trigger.rst