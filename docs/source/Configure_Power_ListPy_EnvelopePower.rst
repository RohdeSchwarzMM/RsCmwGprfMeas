EnvelopePower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:ENPower
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:ENPower:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:ENPower
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:ENPower:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ListPy.EnvelopePower.EnvelopePowerCls
	:members:
	:undoc-members:
	:noindex: