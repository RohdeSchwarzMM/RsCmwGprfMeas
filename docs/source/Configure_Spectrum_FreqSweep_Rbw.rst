Rbw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:RBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:RBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:RBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:RBW



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.FreqSweep.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex: