Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:SPECtrum
	single: STOP:GPRF:MEASurement<Instance>:SPECtrum
	single: ABORt:GPRF:MEASurement<Instance>:SPECtrum

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:SPECtrum
	STOP:GPRF:MEASurement<Instance>:SPECtrum
	ABORt:GPRF:MEASurement<Instance>:SPECtrum



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Average.rst
	Spectrum_Marker.rst
	Spectrum_Maximum.rst
	Spectrum_Minimum.rst
	Spectrum_ReferenceMarker.rst
	Spectrum_Rms.rst
	Spectrum_Sample.rst
	Spectrum_State.rst