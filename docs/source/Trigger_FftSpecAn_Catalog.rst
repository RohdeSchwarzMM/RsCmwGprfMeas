Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:CATalog:SOURce

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:FFTSanalyzer:CATalog:SOURce



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.FftSpecAn.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: