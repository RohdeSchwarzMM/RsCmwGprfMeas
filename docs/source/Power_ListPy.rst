ListPy
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Power.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_ListPy_Average.rst
	Power_ListPy_Current.rst
	Power_ListPy_Maximum.rst
	Power_ListPy_Minimum.rst
	Power_ListPy_Peak.rst
	Power_ListPy_StandardDev.rst