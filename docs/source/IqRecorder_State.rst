State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.IqRecorder.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.iqRecorder.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	IqRecorder_State_All.rst