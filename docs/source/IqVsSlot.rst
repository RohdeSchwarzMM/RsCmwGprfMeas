IqVsSlot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:IQVSlot
	single: STOP:GPRF:MEASurement<Instance>:IQVSlot
	single: ABORt:GPRF:MEASurement<Instance>:IQVSlot

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:IQVSlot
	STOP:GPRF:MEASurement<Instance>:IQVSlot
	ABORt:GPRF:MEASurement<Instance>:IQVSlot



.. autoclass:: RsCmwGprfMeas.Implementations.IqVsSlot.IqVsSlotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.iqVsSlot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	IqVsSlot_FreqError.rst
	IqVsSlot_Icomponent.rst
	IqVsSlot_Level.rst
	IqVsSlot_OfError.rst
	IqVsSlot_Phase.rst
	IqVsSlot_Qcomponent.rst
	IqVsSlot_State.rst