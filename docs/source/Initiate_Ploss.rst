Ploss
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Initiate.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.initiate.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Initiate_Ploss_Evaluate.rst
	Initiate_Ploss_Open.rst
	Initiate_Ploss_Short.rst