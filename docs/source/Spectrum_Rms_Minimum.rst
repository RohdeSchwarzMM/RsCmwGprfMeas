Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:MINimum



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Rms.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: