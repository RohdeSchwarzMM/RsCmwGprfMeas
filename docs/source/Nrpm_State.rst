State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:NRPM:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:NRPM:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.Nrpm.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrpm.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Nrpm_State_All.rst