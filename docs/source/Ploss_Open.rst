Open
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:OPEN

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:OPEN



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Open.OpenCls
	:members:
	:undoc-members:
	:noindex: