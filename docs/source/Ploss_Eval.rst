Eval
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.EvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ploss.eval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ploss_Eval_Frequency.rst
	Ploss_Eval_Gain.rst
	Ploss_Eval_State.rst
	Ploss_Eval_Trace.rst