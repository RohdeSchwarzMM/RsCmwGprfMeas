Trigger
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_FftSpecAn.rst
	Trigger_IqRecorder.rst
	Trigger_IqVsSlot.rst
	Trigger_Power.rst
	Trigger_Spectrum.rst