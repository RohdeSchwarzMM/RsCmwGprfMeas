Gauss
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ParameterSetList.FilterPy.Gauss.GaussCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.parameterSetList.filterPy.gauss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_ParameterSetList_FilterPy_Gauss_Bandwidth.rst