Minimum
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_Minimum_Current.rst
	Power_Minimum_Minimum.rst