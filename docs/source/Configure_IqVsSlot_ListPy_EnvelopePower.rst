EnvelopePower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:ENPower
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:ENPower:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:ENPower
	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:LIST:ENPower:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqVsSlot.ListPy.EnvelopePower.EnvelopePowerCls
	:members:
	:undoc-members:
	:noindex: