Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage
	single: READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage
	READ:GPRF:MEASurement<Instance>:SPECtrum:RMS:AVERage



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Rms.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: