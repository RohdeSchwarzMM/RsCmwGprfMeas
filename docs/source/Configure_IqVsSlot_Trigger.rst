Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQVSlot:TRIGger:SOURce

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQVSlot:TRIGger:SOURce



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqVsSlot.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: