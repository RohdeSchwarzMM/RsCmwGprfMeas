StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	single: READ:GPRF:MEASurement<Instance>:POWer:SDEViation

.. code-block:: python

	CALCulate:GPRF:MEASurement<Instance>:POWer:SDEViation
	FETCh:GPRF:MEASurement<Instance>:POWer:SDEViation
	READ:GPRF:MEASurement<Instance>:POWer:SDEViation



.. autoclass:: RsCmwGprfMeas.Implementations.Power.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.standardDev.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_StandardDev_Current.rst