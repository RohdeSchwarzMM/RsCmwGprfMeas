LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: