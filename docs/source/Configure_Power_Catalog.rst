Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:CATalog:PDEFset

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:CATalog:PDEFset



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: