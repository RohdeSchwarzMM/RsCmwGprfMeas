SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:CMWS:CMODe



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.listPy.singleCmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_ListPy_SingleCmw_Connector.rst