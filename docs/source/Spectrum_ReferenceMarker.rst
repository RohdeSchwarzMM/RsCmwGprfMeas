ReferenceMarker
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_ReferenceMarker_Npeak.rst
	Spectrum_ReferenceMarker_Speak.rst