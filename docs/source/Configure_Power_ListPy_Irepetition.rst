Irepetition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IREPetition
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IREPetition:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IREPetition
	CONFigure:GPRF:MEASurement<Instance>:POWer:LIST:IREPetition:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ListPy.Irepetition.IrepetitionCls
	:members:
	:undoc-members:
	:noindex: