Phase
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQVSlot:PHASe
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:PHASe

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQVSlot:PHASe
	FETCh:GPRF:MEASurement<Instance>:IQVSlot:PHASe



.. autoclass:: RsCmwGprfMeas.Implementations.IqVsSlot.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: