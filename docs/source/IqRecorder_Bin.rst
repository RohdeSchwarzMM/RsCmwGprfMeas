Bin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:IQRecorder:BIN
	FETCh:GPRF:MEASurement<Instance>:IQRecorder:BIN



.. autoclass:: RsCmwGprfMeas.Implementations.IqRecorder.Bin.BinCls
	:members:
	:undoc-members:
	:noindex: