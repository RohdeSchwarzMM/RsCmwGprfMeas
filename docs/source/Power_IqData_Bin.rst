Bin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:IQData:BIN

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:IQData:BIN



.. autoclass:: RsCmwGprfMeas.Implementations.Power.IqData.Bin.BinCls
	:members:
	:undoc-members:
	:noindex: