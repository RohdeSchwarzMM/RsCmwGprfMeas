IqRecorder
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:IQRecorder
	single: ABORt:GPRF:MEASurement<Instance>:IQRecorder
	single: STOP:GPRF:MEASurement<Instance>:IQRecorder
	single: READ:GPRF:MEASurement<Instance>:IQRecorder
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:IQRecorder
	ABORt:GPRF:MEASurement<Instance>:IQRecorder
	STOP:GPRF:MEASurement<Instance>:IQRecorder
	READ:GPRF:MEASurement<Instance>:IQRecorder
	FETCh:GPRF:MEASurement<Instance>:IQRecorder



.. autoclass:: RsCmwGprfMeas.Implementations.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	IqRecorder_Bin.rst
	IqRecorder_Reliability.rst
	IqRecorder_State.rst
	IqRecorder_SymbolRate.rst
	IqRecorder_Talignment.rst