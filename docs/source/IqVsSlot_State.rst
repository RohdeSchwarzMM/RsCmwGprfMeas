State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQVSlot:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQVSlot:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.IqVsSlot.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.iqVsSlot.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	IqVsSlot_State_All.rst