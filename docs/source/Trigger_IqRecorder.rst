IqRecorder
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:OFFSet
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:MGAP
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:THReshold
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTHreshold
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTime
	single: TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SLOPe

.. code-block:: python

	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SOURce
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:OFFSet
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:MGAP
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:TOUT
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:THReshold
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTHreshold
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:PCTime
	TRIGger:GPRF:MEASurement<Instance>:IQRecorder:SLOPe



.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.IqRecorder.IqRecorderCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.iqRecorder.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_IqRecorder_Catalog.rst