State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:STATe

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:PLOSs:EVAL:STATe



.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.State.StateCls
	:members:
	:undoc-members:
	:noindex: