Vbw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:VBW



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.FreqSweep.Vbw.VbwCls
	:members:
	:undoc-members:
	:noindex: