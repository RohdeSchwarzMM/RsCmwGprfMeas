Trace
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Ploss.Eval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ploss.eval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ploss_Eval_Trace_Frequency.rst
	Ploss_Eval_Trace_Gain.rst