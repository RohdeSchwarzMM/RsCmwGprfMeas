ParameterSetList
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:POWer:PSET

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:POWer:PSET



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ParameterSetList.ParameterSetListCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.parameterSetList.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_ParameterSetList_Catalog.rst
	Configure_Power_ParameterSetList_FilterPy.rst
	Configure_Power_ParameterSetList_Mlength.rst
	Configure_Power_ParameterSetList_PdefSet.rst
	Configure_Power_ParameterSetList_Slength.rst