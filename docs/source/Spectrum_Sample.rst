Sample
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.Sample.SampleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.spectrum.sample.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Spectrum_Sample_Average.rst
	Spectrum_Sample_Current.rst
	Spectrum_Sample_Maximum.rst
	Spectrum_Sample_Minimum.rst