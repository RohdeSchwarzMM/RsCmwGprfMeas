Reliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:IQRecorder:RELiability

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:IQRecorder:RELiability



.. autoclass:: RsCmwGprfMeas.Implementations.IqRecorder.Reliability.ReliabilityCls
	:members:
	:undoc-members:
	:noindex: