Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency
	single: CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency:ALL

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency
	CONFigure:GPRF:MEASurement<Instance>:IQRecorder:LIST:FREQuency:ALL



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.IqRecorder.ListPy.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: