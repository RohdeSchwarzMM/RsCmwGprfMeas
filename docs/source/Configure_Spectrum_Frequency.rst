Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:CENTer
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STARt
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STOP
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:LASPan

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:CENTer
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STARt
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:STOP
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FREQuency:LASPan



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.spectrum.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Spectrum_Frequency_Span.rst