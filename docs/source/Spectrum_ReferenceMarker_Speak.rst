Speak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:SPECtrum:REFMarker:SPEak



.. autoclass:: RsCmwGprfMeas.Implementations.Spectrum.ReferenceMarker.Speak.SpeakCls
	:members:
	:undoc-members:
	:noindex: