Maiq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:GPRF:MEASurement<Instance>:SCENario:MAIQ

.. code-block:: python

	ROUTe:GPRF:MEASurement<Instance>:SCENario:MAIQ



.. autoclass:: RsCmwGprfMeas.Implementations.Route.Scenario.Maiq.MaiqCls
	:members:
	:undoc-members:
	:noindex: