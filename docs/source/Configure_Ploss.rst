Ploss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:PLOSs:TRACe

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:PLOSs:TRACe



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Ploss.PlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ploss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ploss_ListPy.rst
	Configure_Ploss_Mpath.rst
	Configure_Ploss_View.rst