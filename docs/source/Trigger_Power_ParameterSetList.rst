ParameterSetList
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Trigger.Power.ParameterSetList.ParameterSetListCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.power.parameterSetList.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Power_ParameterSetList_Offset.rst