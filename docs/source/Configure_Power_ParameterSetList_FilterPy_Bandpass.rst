Bandpass
----------------------------------------





.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Power.ParameterSetList.FilterPy.Bandpass.BandpassCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.power.parameterSetList.filterPy.bandpass.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Power_ParameterSetList_FilterPy_Bandpass_Bandwidth.rst