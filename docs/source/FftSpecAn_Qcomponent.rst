Qcomponent
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:GPRF:MEASurement<Instance>:FFTSanalyzer:Q
	single: FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:Q

.. code-block:: python

	READ:GPRF:MEASurement<Instance>:FFTSanalyzer:Q
	FETCh:GPRF:MEASurement<Instance>:FFTSanalyzer:Q



.. autoclass:: RsCmwGprfMeas.Implementations.FftSpecAn.Qcomponent.QcomponentCls
	:members:
	:undoc-members:
	:noindex: