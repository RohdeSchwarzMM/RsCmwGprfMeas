Short
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: INITiate:GPRF:MEASurement<Instance>:PLOSs:SHORt

.. code-block:: python

	INITiate:GPRF:MEASurement<Instance>:PLOSs:SHORt



.. autoclass:: RsCmwGprfMeas.Implementations.Initiate.Ploss.Short.ShortCls
	:members:
	:undoc-members:
	:noindex: