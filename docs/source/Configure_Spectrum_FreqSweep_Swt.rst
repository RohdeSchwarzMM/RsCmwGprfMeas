Swt
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	single: CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT

.. code-block:: python

	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT:AUTO
	CONFigure:GPRF:MEASurement<Instance>:SPECtrum:FSWeep:SWT



.. autoclass:: RsCmwGprfMeas.Implementations.Configure.Spectrum.FreqSweep.Swt.SwtCls
	:members:
	:undoc-members:
	:noindex: