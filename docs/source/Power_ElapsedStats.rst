ElapsedStats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:ESTatistics

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:ESTatistics



.. autoclass:: RsCmwGprfMeas.Implementations.Power.ElapsedStats.ElapsedStatsCls
	:members:
	:undoc-members:
	:noindex: