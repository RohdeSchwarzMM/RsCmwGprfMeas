CumulativeDistribFnc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:GPRF:MEASurement<Instance>:POWer:CCDF

.. code-block:: python

	FETCh:GPRF:MEASurement<Instance>:POWer:CCDF



.. autoclass:: RsCmwGprfMeas.Implementations.Power.CumulativeDistribFnc.CumulativeDistribFncCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.power.cumulativeDistribFnc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Power_CumulativeDistribFnc_Power.rst
	Power_CumulativeDistribFnc_Probability.rst
	Power_CumulativeDistribFnc_Sample.rst